import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button } from 'react-native-elements';

export default class App extends React.Component {
  render() {
    return (
      <Image
     style={{
       flex: 1,
       width:100,
       height:100,
     }}
     source={require('../assets/rose.jpeg')}
     />
      <View style={styles.container}>
        <Text>Hello World</Text>
        <Button title='Go' icon={{name:'code'}} onPress={(
          Alert.alert('Welcometo hello world')
        )}>
        
        
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    image: {
      uri: './assets/rose.jpeg',
      height: 576,
      width: 1024
  },
  },
});
